EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:iForninhoPCB-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
NoConn ~ 4100 2100
NoConn ~ 4100 2200
NoConn ~ 4100 2300
NoConn ~ 4100 2400
NoConn ~ 4100 2500
NoConn ~ 4100 2600
NoConn ~ 4100 2700
NoConn ~ 4100 2800
NoConn ~ 4100 2900
NoConn ~ 4100 3000
NoConn ~ 4100 3100
NoConn ~ 4100 3200
NoConn ~ 4100 3300
NoConn ~ 4100 3400
NoConn ~ 4100 3500
NoConn ~ 5300 2500
NoConn ~ 5300 3100
NoConn ~ 5300 3300
NoConn ~ 5300 3400
NoConn ~ 5300 3500
$Comp
L CONN_01X05 P4
U 1 1 59661A11
P 6700 2850
F 0 "P4" H 6700 3150 50  0000 C CNN
F 1 "CONN_01X05" V 6800 2850 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x05_Pitch2.54mm" H 6700 2850 50  0001 C CNN
F 3 "" H 6700 2850 50  0000 C CNN
	1    6700 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 2700 5300 2700
Wire Wire Line
	6000 1750 6000 3150
Wire Wire Line
	6000 2650 6500 2650
Wire Wire Line
	5300 2600 5900 2600
Wire Wire Line
	5900 2300 5900 3200
Wire Wire Line
	5900 2750 6500 2750
Wire Wire Line
	5300 2800 6400 2800
Wire Wire Line
	6400 2800 6400 2850
Wire Wire Line
	6400 2850 6500 2850
Wire Wire Line
	5300 2900 6250 2900
Wire Wire Line
	6250 2900 6250 3050
Wire Wire Line
	6250 3050 6500 3050
Wire Wire Line
	5300 3000 6400 3000
Wire Wire Line
	6400 3000 6400 2950
Wire Wire Line
	6400 2950 6500 2950
$Comp
L CONN_01X04 P3
U 1 1 59661E13
P 6700 2250
F 0 "P3" H 6700 2500 50  0000 C CNN
F 1 "CONN_01X04" V 6800 2250 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x04_Pitch2.54mm" H 6700 2250 50  0001 C CNN
F 3 "" H 6700 2250 50  0000 C CNN
	1    6700 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 2300 5800 2300
Wire Wire Line
	5800 2300 5800 2100
Wire Wire Line
	5800 2100 6500 2100
Wire Wire Line
	5300 2200 6500 2200
Wire Wire Line
	6500 2300 5900 2300
Connection ~ 5900 2600
Wire Wire Line
	6500 2400 6000 2400
Connection ~ 6000 2650
$Comp
L CONN_01X02 P2
U 1 1 59662401
P 6700 1700
F 0 "P2" H 6700 1850 50  0000 C CNN
F 1 "CONN_01X02" V 6800 1700 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_Pheonix_PT-3.5mm_2pol" H 6700 1700 50  0001 C CNN
F 3 "" H 6700 1700 50  0000 C CNN
	1    6700 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 1650 6500 1650
Connection ~ 6000 2400
Wire Wire Line
	6000 1750 6500 1750
Wire Wire Line
	5500 1650 5500 2400
Wire Wire Line
	5500 2400 5300 2400
NoConn ~ 5300 2100
$Comp
L R R1
U 1 1 5966290C
P 5750 3200
F 0 "R1" V 5830 3200 50  0000 C CNN
F 1 "1k" V 5750 3200 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 5680 3200 50  0001 C CNN
F 3 "" H 5750 3200 50  0000 C CNN
	1    5750 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	5600 3200 5300 3200
Connection ~ 5900 2750
$Comp
L CONN_01X02 P1
U 1 1 596629A4
P 5900 3650
F 0 "P1" H 5900 3800 50  0000 C CNN
F 1 "CONN_01X02" V 6000 3650 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_Pheonix_PT-3.5mm_2pol" H 5900 3650 50  0001 C CNN
F 3 "" H 5900 3650 50  0000 C CNN
	1    5900 3650
	0    1    1    0   
$EndComp
Wire Wire Line
	5950 3450 5950 3150
Wire Wire Line
	5950 3150 6000 3150
Connection ~ 6000 2700
Wire Wire Line
	5850 3450 5500 3450
Wire Wire Line
	5500 3450 5500 3200
Connection ~ 5500 3200
$Comp
L NodeMCU U1
U 1 1 5967B5D7
P 4700 2800
F 0 "U1" H 4700 2900 60  0000 C CNN
F 1 "NodeMCU" H 4700 2800 60  0000 C CNN
F 2 "ESP8266:NodeMCU1.0(12-E)" H 4700 2900 60  0001 C CNN
F 3 "" H 4700 2900 60  0001 C CNN
	1    4700 2800
	1    0    0    -1  
$EndComp
$EndSCHEMATC
