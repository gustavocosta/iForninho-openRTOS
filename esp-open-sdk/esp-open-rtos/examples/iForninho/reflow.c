#include "reflow.h"

// ***** PID CONTROL VARIABLES *****
float setpoint;
float input;
float output;
float error = 0;
float prevError = 0;
float intError = 0;
float difError = 0;
float kp = PID_KP_PREHEAT;
float ki = PID_KI_PREHEAT;
float kd = PID_KD_PREHEAT;
uint16_t timerSoak;
int isReflowing = 0;
uint16_t pidMaxLim;
uint16_t initialTime = 0;

uint8_t debugging = 0;
reflow_state_t previousState = NULL_STATE;

/* Solid State RELAY pin */
int ssrPin;

void reflow_init(uint16_t currentRuntime, int pin)
{
  ssrPin = pin;
  initialTime = currentRuntime;
  reflowState = IDLE;
  input = 25;
  isReflowing = 1;
}

reflow_state_t reflow_exec(uint16_t currentRuntime, float temperature)
{
      input = (float)temperature;

      state_machine(input, currentRuntime);

      if(previousState != reflowState){
        debugging = 1;
      } else {
        debugging = 0;
      }

      previousState = reflowState;

      if(debugging){
        printf("Temp: %.2f° ", input);
        printf("| Setpoint: %.2f°\n", setpoint);
      }

      ssr_control();

      return reflowState;
}

void state_machine(float currentTemperature, uint16_t currentRuntime)
{
  switch (reflowState){
    case IDLE:
    // If oven temperature is still above room temperature
    // If switch is pressed to start reflow process
    if (isReflowing == 1) {
      if (currentTemperature >= TEMPERATURE_ROOM) {
        reflowState = TOO_HOT;
      } else {
        isReflowing = 0;
        if(debugging)
          printf("Initializing reflow oven...\n");
        // Ramp up to minimum soaking temperature
        setpoint = TEMPERATURE_SOAK_MIN + 10;
        // Tell the PID to range between 0 and the full window size
        pidMaxLim = WINDOW_SIZE;
        kp = PID_KP_PREHEAT;
        ki = PID_KI_PREHEAT;
        kd = PID_KD_PREHEAT;
        // Proceed to preheat stage
        reflowState = PREHEAT;
      }
    } else {
      if(debugging)
        printf("Off | ");
    }
    break;

    case PREHEAT:
    if(debugging)
      printf("Preheat | ");
    ovenStatus = ON;
    // If minimum soak temperature is achieve
    if (currentTemperature >= TEMPERATURE_SOAK_MIN) {
      // Chop soaking period into smaller sub-period
      timerSoak = currentRuntime + SOAK_MICRO_PERIOD;
      // Set less agressive PID parameters for soaking ramp
      kp = PID_KP_SOAK;
      ki = PID_KI_SOAK;
      kd = PID_KD_SOAK;
      // Ramp up to first section of soaking temperature
      setpoint = TEMPERATURE_SOAK_MIN + SOAK_TEMPERATURE_STEP + 10;
      // Proceed to soaking state
      reflowState = SOAK;
    }
    break;

    case SOAK:
    if(debugging)
      printf("Soak | ");
    // If micro soak temperature is achieved
    if (currentRuntime > timerSoak)
    {
      timerSoak = currentRuntime + SOAK_MICRO_PERIOD;
      // Increment micro setpoint
      setpoint += SOAK_TEMPERATURE_STEP;
      if (setpoint > TEMPERATURE_SOAK_MAX) {
        // Set agressive PID parameters for reflow ramp
        kp = PID_KP_REFLOW;
        ki = PID_KI_REFLOW;
        kd = PID_KD_REFLOW;
        // Ramp up to first section of soaking temperature
        setpoint = TEMPERATURE_REFLOW_MAX + 10;
        // Proceed to reflowing state
        reflowState = REFLOW;
      }
    }
    break;

    case REFLOW:
    if(debugging)
      printf("Reflow | ");
    // We need to avoid hovering at peak temperature for too long
    // Crude method that works like a charm and safe for the components
    if (currentTemperature >= (TEMPERATURE_REFLOW_MAX - 5)) {
      // Set PID parameters for cooling ramp
      kp = PID_KP_REFLOW;
      ki = PID_KI_REFLOW;
      kd = PID_KD_REFLOW;
      // Ramp down to minimum cooling temperature
      setpoint = TEMPERATURE_COOL_MIN - 20;
      // Proceed to cooling state
      reflowState = COOL;
    }
    break;

    case COOL:
    if(debugging)
      printf("Cool | ");
    // If minimum cool temperature is achieve
    if (currentTemperature <= TEMPERATURE_COOL_MIN) {
      // Turn off reflow process
      ovenStatus = OFF;
      // Proceed to reflow Completion state
      reflowState = COMPLETE;
    }
    break;

    case COMPLETE:
    if(debugging)
      printf("Complete | ");
    ovenStatus = OFF;
    // Reflow process ended
    reflowState = IDLE;
    break;

    case TOO_HOT:
    // If oven temperature drops below room temperature
    if (currentTemperature < TEMPERATURE_ROOM) {
      if(debugging)
        printf("Too hot | ");
      // Ready to reflow
      reflowState = IDLE;
    }
    break;
  }

}

void pid()
{
  error = setpoint - input;
  intError += (ki * error);
  if(intError > pidMaxLim)
  intError = pidMaxLim;
  if(intError < 0)
  intError = 0;
  difError = error - prevError;

  prevError = error;

  output = (kp * error) + intError - (kd * difError);

  if(output > pidMaxLim)
  output = pidMaxLim;
  if(output < 0)
  output = 0;
}

void ssr_control()
{
  if (ovenStatus == ON) {
    pid();
    if(debugging)
      printf(" PID: %.2f | ", output);

    if(output >= 1000){
      gpio_write(ssrPin, 1);
    }
    else{
      gpio_write(ssrPin, 0);
    }

  } else {
    gpio_write(ssrPin, 0);
  }
}
