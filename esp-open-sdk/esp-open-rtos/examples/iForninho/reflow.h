#include <max6675/max6675.h>
#include <FreeRTOS.h>
#include <task.h>
#include <stdio.h>

#ifndef REFLOW_H
#define REFLOW_H

/* Thermocouple parameters */
#define	MAX6675_SO		12
#define MAX6675_CS		13
#define MAX6675_SCK		14

#define TEMPERATURE_ROOM 50
#define TEMPERATURE_SOAK_MIN 100
#define TEMPERATURE_SOAK_MAX 150
#define TEMPERATURE_REFLOW_MAX 205
#define TEMPERATURE_COOL_MIN 100
#define SENSOR_SAMPLING_TIME 1000
#define SOAK_TEMPERATURE_STEP 5
#define SOAK_MICRO_PERIOD 9000
#define WINDOW_SIZE  2000;

// ***** PID PARAMETERS *****
// ***** PRE-HEAT STAGE *****
#define PID_KP_PREHEAT 100
#define PID_KI_PREHEAT 0.025
#define PID_KD_PREHEAT 20
// ***** SOAKING STAGE *****
#define PID_KP_SOAK 300
#define PID_KI_SOAK 0.05
#define PID_KD_SOAK 250
// ***** REFLOW STAGE *****
#define PID_KP_REFLOW 300
#define PID_KI_REFLOW 0.05
#define PID_KD_REFLOW 350
#define PID_SAMPLE_TIME 1000

typedef enum REFLOW_STATE
{
  IDLE,
  PREHEAT,
  SOAK,
  REFLOW,
  COOL,
  COMPLETE,
  TOO_HOT,
  NULL_STATE
} reflow_state_t;

typedef enum OVEN_STATUS
{
  OFF,
  ON
} oven_status_t;

// Reflow oven controller state machine state variable
reflow_state_t reflowState;
// Reflow oven controller status
oven_status_t ovenStatus;

void reflow_init(uint16_t currentRuntime, int pin);
reflow_state_t reflow_exec(uint16_t currentRuntime, float temperature);
void state_machine(float currentTemperature, uint16_t currentRuntime);
void pid(void);
void ssr_control(void);

#endif // MAIN_H
