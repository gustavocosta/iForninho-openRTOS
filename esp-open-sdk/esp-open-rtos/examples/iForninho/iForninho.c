#include <espressif/esp_common.h>
#include <esp/uart.h>
#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <timers.h>
#include <string.h>
#include <ssd1306/ssd1306.h>
#include <reflow.h>
#include <i2c/i2c.h>
#include "fonts/fonts.h"
#include "queue.h"
#include <stdio.h>

/* Display size */
#define DISPLAY_WIDTH  128
#define DISPLAY_HEIGHT 64

/* Time of execution of the whole process */
#define TOTAL_TIME_EXEC 452

/* Display font */
#define DEFAULT_FONT font_builtin_fonts[FONT_FACE_TERMINUS_6X12_ISO8859_1]

/* Temperature icon coordinates and size */
#define TEMPERATURE_ICON_X 64 // (int)((3/4)*DISPLAY_WIDTH)
#define TEMPERATURE_ICON_Y 0 // (int)((3/4)*DISPLAY_HEIGHT)
#define TEMPERATURE_ICON_SIZE font_builtin_fonts[(font_face_t) 8]

/* Timer icon coordinates and size */
#define TIMER_ICON_X 24 // (int)((3/4)*DISPLAY_WIDTH)
#define TIMER_ICON_Y 0 // (int)((3/4)*DISPLAY_HEIGHT)
#define TIMER_ICON_SIZE font_builtin_fonts[(font_face_t) 8]

/* Display I2C communication parameters */
#define PROTOCOL SSD1306_PROTO_I2C
#define ADDR     SSD1306_I2C_ADDR_0
#define DISPLAY_SCL    5    // White wire
#define DISPLAY_SDA    4    // Yellow wire

/* Thermocouple parameters */
#define	MAX6675_SO		12
#define MAX6675_CS		13
#define MAX6675_SCK		14

/* Solid State RELAY pin */
#define SSR_PIN       0

/* Turn-On Button pin */
#define BUTTON_PIN    3

#define SECOND (20 / portTICK_PERIOD_MS)

/* Display descriptor */
static const ssd1306_t display = {
  .protocol = PROTOCOL,
  .addr     = ADDR,
  .width    = DISPLAY_WIDTH,
  .height   = DISPLAY_HEIGHT
};

/* Thermocouple descriptor */
/*
static const max6675_thermocouple_t thermocouple = {
  .SO = MAX6675_SO;
  .CS = MAX6675_CS;
  .SCK = MAX6675_SCK;
};
*/

/* Local frame buffer */
static uint8_t buffer[DISPLAY_WIDTH * DISPLAY_HEIGHT / 8];

TimerHandle_t clock_per_second_handle = NULL;


/* Graph coordinates */
float graphX = 0;
uint16_t graphY = 15;

float temperature = 25;

uint16_t timer = 0;

reflow_state_t reflowState = IDLE;

long map(long x, long in_min, long in_max, long out_min, long out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

static void display_task(void *pvParameters)
{
  printf("%s: Started user interface task\n", __FUNCTION__);
  vTaskDelay(SECOND);

  ssd1306_set_whole_display_lighting(&display, false);

  char aux_text[20];

  while (1) {
    if (reflowState == COMPLETE){
      ssd1306_draw_string(&display, buffer, DEFAULT_FONT, 0, 40, "SOLDERING COMPLETE!", OLED_COLOR_WHITE, OLED_COLOR_BLACK);
    } else if(reflowState == IDLE) {
      ssd1306_draw_string(&display, buffer, DEFAULT_FONT, 0, 28, "iForninho Reflow Oven", OLED_COLOR_WHITE, OLED_COLOR_BLACK);
    } else {

      // Ploting temperature graph
      ssd1306_draw_hline(&display, buffer, 0, graphY-1, DISPLAY_WIDTH, OLED_COLOR_WHITE);

      // Clean display if X goes out of borders
      if (graphX >= 129) {
        graphX = 0;
        ssd1306_fill_rectangle(&display, buffer, 0, graphY, DISPLAY_WIDTH, DISPLAY_HEIGHT, OLED_COLOR_BLACK);
      }

      int maxTemperature = 220;
      int minTemperature = 20;
      int h = (int) map(temperature, minTemperature, maxTemperature, DISPLAY_HEIGHT-graphY, 0);

      ssd1306_draw_vline(&display, buffer, graphX, graphY, DISPLAY_HEIGHT-graphY, OLED_COLOR_WHITE);
      ssd1306_draw_vline(&display, buffer, graphX, graphY, h, OLED_COLOR_BLACK);


      // Time icon
      sprintf(aux_text, "%us", timer);
      ssd1306_draw_string(&display, buffer, TIMER_ICON_SIZE, TIMER_ICON_X, TIMER_ICON_Y, aux_text, OLED_COLOR_WHITE, OLED_COLOR_BLACK);

      // Temperature icon
      sprintf(aux_text, "%u\xf8", (uint16_t) temperature);
      ssd1306_draw_string(&display, buffer, TEMPERATURE_ICON_SIZE, TEMPERATURE_ICON_X, TEMPERATURE_ICON_Y, aux_text, OLED_COLOR_WHITE, OLED_COLOR_BLACK);
    }

    if (ssd1306_load_frame_buffer(&display, buffer))
        goto error_loop;

  }

  error_loop:
  printf("%s: error while loading framebuffer into SSD1306\n", __func__);
  for (;;) {
    vTaskDelay(2 * SECOND);
    printf("%s: error loop\n", __FUNCTION__);
  }
}

float simulatedSensor() {
  float simulatedTemperature = temperature;
  if (gpio_read(SSR_PIN) == 1) {
    simulatedTemperature += 0.5;
  } else if (simulatedTemperature > 25) {
    simulatedTemperature -= 1;
  }
  return simulatedTemperature;
}

void clock_per_second_task(TimerHandle_t h)
{
  temperature = simulatedSensor(); // max6675_read(&thermocouple);
  graphX += (float) DISPLAY_WIDTH/TOTAL_TIME_EXEC;
  timer++;
  reflowState = reflow_exec(timer*1000, temperature);
}

void buttonPollTask(void *pvParameters)
{
    while(1) {
        while (gpio_read(BUTTON_PIN) != 0)
        {
            taskYIELD();
        }
        // Debouncing
        vTaskDelay(200 / portTICK_PERIOD_MS);

        // printf("Button pressed at %dms\r\n", xTaskGetTickCount()*portTICK_PERIOD_MS);

        graphX = 0;

        timer = 0;
        temperature = 25;
        // Initialize Reflow process
        reflow_init(timer, SSR_PIN);

        // Clean display
        vTaskDelay(200 / portTICK_PERIOD_MS);
        ssd1306_fill_rectangle(&display, buffer, 0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT, OLED_COLOR_BLACK);
    }
}

void user_init(void)
{
  //uncomment to test with CPU overclocked
  //sdk_system_update_cpu_freq(160);

  // Setup HW
  uart_set_baud(0, 115200);

  printf("SDK version:%s\n", sdk_system_get_sdk_version());

  // Deactivate WiFi to prevent "pause" on boot and save the planet. I mean power.
  sdk_wifi_set_opmode(NULL_MODE);

  // Initialize the MAX6675 thermocouple interface
  max6675_init();

  i2c_init(DISPLAY_SCL, DISPLAY_SDA);

  gpio_enable(SSR_PIN, GPIO_OUTPUT);
  gpio_enable(BUTTON_PIN, GPIO_INPUT);

  gpio_write(SSR_PIN, 0);

  while (ssd1306_init(&display) != 0) {
    printf("%s: failed to init SSD1306 lcd\n", __func__);
    vTaskDelay(SECOND);
  }
  ssd1306_set_whole_display_lighting(&display, true);

  vTaskDelay(SECOND);

  // Create user interface task
  xTaskCreate(display_task, "display_task", 256, NULL, 1, NULL);

  clock_per_second_handle = xTimerCreate("clock_per_second_task", SECOND, pdTRUE, NULL, clock_per_second_task);
  xTimerStart(clock_per_second_handle, 0);

  xTaskCreate(buttonPollTask, "buttonPollTask", 256, NULL, 1, NULL);
}
