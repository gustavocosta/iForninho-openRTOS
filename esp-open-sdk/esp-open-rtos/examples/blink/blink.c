#include <math.h>
#include <stdlib.h>
#include "espressif/esp_common.h"
#include "esp/uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "esp8266.h"


const int GPIO16  = 14;
const int GPIO14  = 12;
const int GPIO12  = 13;
int ADC = 0;

void delay(int ms) {
  // ms = milissegundos
  vTaskDelay(ms / portTICK_PERIOD_MS);
}

float temperatura(){
  int nominalThermisterTemperature = 1000;
  int nominalTemperature = 25;
  int numberOfSamples = 5;
  int seriesResistor = 1000;
  int BCoefficient = 3600;
  int samples[numberOfSamples];
  float mean = 0;

  for (int i = 0; i < numberOfSamples; i++) {
    samples[i] = sdk_system_adc_read();
    delay(10);
  }
  for (int i=0; i< numberOfSamples; i++) {
    mean += samples[i];
  }
  mean /= numberOfSamples;

  // Convert the thermal stress value to resistance
  mean = 1023 / mean - 1;
  mean = seriesResistor / mean;

  //Calculate temperature using the Beta Factor equation
  // (1/(1/B * ln(R/Ro) + (1/To))) - 273.15
  float temperature = (log(mean / nominalThermisterTemperature) / BCoefficient);
  temperature += (1 / (nominalTemperature + 273));
  temperature = 1 / temperature;
  temperature -= 273.15;                         // Convert it to Celsius
  return temperature;
}

void GPIOTest(void *pvParameters) {

  // configure pins and setting them low
  //pinMode(A0, INPUT);
  gpio_enable(GPIO16, GPIO_OUTPUT);
  gpio_enable(GPIO14, GPIO_OUTPUT);
  gpio_enable(GPIO12, GPIO_OUTPUT);
  gpio_write(GPIO16, 0);
  gpio_write(GPIO14, 0);
  gpio_write(GPIO12, 0);

  while(1) {
      // Print the ADC value with all pins low
      printf("LOW start: ");
      ADC = sdk_system_adc_read();
      printf("%d\n", ADC);


      // Send voltage to GPIO 16 completing the circuit for this sensor.
      gpio_write(GPIO16,     1);
      // short delay before reading, 100ms
      delay(1000);
      // Read the ADC pin or assign to varable var = adc.read(0)
      printf("D5 | LDR: ");
      ADC = sdk_system_adc_read();
      printf("%d\n", ADC);
      // Return GPIO 16 to LOW, no voltage.
      gpio_write(GPIO16,     0);
      // short delay 100ms
      delay(1000);
      // repeat for the other two sensors


      gpio_write(GPIO14,     1);
      delay(1000);
      printf("D6 | Temp: ");
      //ADC = sdk_system_adc_read();
      printf("%f\n", temperatura());
      gpio_write(GPIO14,     0);
      delay(1000);

      gpio_write(GPIO12,     1);
      delay(1000);
      printf("D7 | Moisture: ");
      ADC = sdk_system_adc_read();
      if(ADC < 5)
        ADC = 5;
      else if(ADC > 364)
        ADC = 364;
      ADC = 100*(ADC-364)/(5-364);
      printf("%d\n", ADC);
      gpio_write(GPIO12,     0);
      delay(1000);

      //  again read ADC, this reading should be very close to the 'start reading'
      printf("LOW end: ");
      ADC = sdk_system_adc_read();
      printf("%d\n\n\n", ADC);
      delay(1000);
    }
}

void WIFITest(void *pvParameters) {
  while(1) {
    return;
  }
}

void user_init(void)
{
    uart_set_baud(0, 115200);
    //xTaskCreate(GPIOTest, "GPIOTestTask", 256, NULL, 2, NULL);
    xTaskCreate(GPIOTest, "GPIOTestTask", 256, NULL, 2, NULL);
    //xTaskCreate(blinkenRegisterTask, "blinkenRegisterTask", 256, NULL, 2, NULL);
}
