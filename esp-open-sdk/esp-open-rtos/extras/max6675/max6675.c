/*
 *  Simple bit-banging library for reading temperature samples off a
 *  MAX6675 and attached K-type thermocouple.
 *
 *  Call the "max6675_init" function to setup the ESP8266 I/O pins prior to
 *  reading the chip.
 *
 *  Call the "max6675_read" function to grab the contents of the MAX6675 internal
 *  register, which is a 16-bit value. See the example code in "user_main.c"
 *  or read the MAX6675 datasheet for a complete description of the return value.
 *
 *  Visit Nefastor Online for more information, including the article connected to
 *  this code, which describes hardware connections and more :
 *
 */

/*
The MIT License (MIT)
Copyright (c) 2016 Nefastor Online

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use, copy,
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the
following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "max6675.h"

#include "espressif/esp_common.h"	// includes pin_mux_register.h
#include <esp/gpio.h>

/* Thermocouple descriptor */
static max6675_thermocouple_t thermocouple = {
  .SO   = MAX6675_SO,
  .CS   = MAX6675_CS,
  .SCK  = MAX6675_SCK
};

// Set GPIO pins (D2 as input for SO, D3 and D4 and outputs for CS# and SCK respectively)
// D2 is GPIO 4, D3 is GPIO 0, D4 is GPIO 2.
void max6675_init()
{
	// Now set the direction of each pin, using gpio.h macros
	gpio_enable(thermocouple.CS, GPIO_OUTPUT);
	gpio_enable(thermocouple.SCK, GPIO_OUTPUT);
	gpio_enable(thermocouple.SO, GPIO_INPUT);

	// Finally, set the outputs' default state (CS = 1, SCK = 0)
	gpio_write(thermocouple.CS, 1);
	gpio_write(thermocouple.SCK, 0);
}

uint16_t max6675_read()
{
	// Start reading a sample by lowering CS#
	gpio_write(thermocouple.CS, 0);

	// Send 16 SCK pulses and read SO each time
	int pulse = 16;
	uint16_t readValue = 0;

	while (pulse--)
	{
		readValue = readValue << 1;					// shift the result to clear the LSB for the next bit
		readValue |= gpio_read(thermocouple.SO);	// read the SO pin

		gpio_write(thermocouple.SCK, 1);		// start a clock pulse
		gpio_write(thermocouple.SCK, 0);		// end a clock pulse
	}

	// After the read is complete, set CS# back up (starts a new sampling cycle)
	gpio_write(thermocouple.CS, 1);

	// only bits 14 down to 3 represent a temperature sample
	uint16_t temperature = readValue >> 3;	// eliminate the 3 LSB. Bit 15 is always zero, leave it
	// 1 LSB equals 1/4th of a degree Centigrade, thus a raw conversion into Centigrades is:
	temperature = temperature >> 2;

	return temperature;
}
