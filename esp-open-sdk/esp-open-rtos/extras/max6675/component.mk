# include it as 'max6675/max6675.h'
INC_DIRS += $(max6675_ROOT)..

# args for passing into compile rule generation
max6675_INC_DIR =
max6675_SRC_DIR = $(max6675_ROOT)

$(eval $(call component_compile_rules,max6675))
