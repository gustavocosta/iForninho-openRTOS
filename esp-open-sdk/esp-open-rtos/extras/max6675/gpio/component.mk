# Component makefile for extras/gpio

# expected anyone using GPIO driver includes it as 'gpio/gpio.h'
INC_DIRS += $(gpio_ROOT)..

# args for passing into compile rule generation
gpio_INC_DIR =
gpio_SRC_DIR = $(gpio_ROOT)

$(eval $(call component_compile_rules,gpio))
