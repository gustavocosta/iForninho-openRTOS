/*
 * max6675.h
 *
 *  Created on: Aug 20, 2016
 *      Author: Nefastor
 *
*/

/*
The MIT License (MIT)
Copyright (c) 2016 Nefastor Online

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use, copy,
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the
following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef EXTRAS_MAX6675_H_
#define EXTRAS_MAX6675_H_

//#include <c_types.h>
#include <stdint.h>

/* Thermocouple parameters */
#define	MAX6675_SO		12
#define MAX6675_CS		13
#define MAX6675_SCK		14

/**
 * Thermocouple descriptor
 */
typedef struct
{
    uint8_t SO;
    uint8_t CS;
    uint8_t SCK;
} max6675_thermocouple_t;

void max6675_init();		// mux and set I/O pins for MAX6675 operation

uint16_t max6675_read();		// read 16 bits from a MAX6675

#endif /* EXTRAS_MAX6675_H_ */
